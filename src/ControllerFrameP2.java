import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;


public class ControllerFrameP {
	
	public FrameP f;
	public Game g;
	public String name;
	
	public ControllerFrameP(FrameP f, Game g, String name) {
		this.f = f;
		this.g = g;
		this.name = name;
		this.g.getPlayer().setName(this.name);
	
	this.f.addFramePListener(new FramePListener());
	}
	
	public void updateFrame() {
		f.setPicture(g.getTries());
		f.setInfo(transform(g.getPlayer().getName().toString(), g.getPlayer().getFinalPoints(), g.getTries());
		f.setMW(g.wordGame().toString());
		//f.revalidate();
		//System.out.println("UPDATE");
	}
	
	public void theEnd() {
		JOptionPane jop1 = new JOptionPane();//on informe du mot et des points gagn�s
		jop1.showMessageDialog(null,"Le mot recherch� �tait "+g.getWord().getString()+" , votre score est de "+g.getPlayer().getFinalPoints()+ " points", "Game Over", JOptionPane.INFORMATION_MESSAGE);
		f.dispose();//remettre les points avant de fermer
	}
	
	public void frameWin() {
		JOptionPane jop1 = new JOptionPane();//on informe du mot et des points gagn�s
		jop1.showMessageDialog(null,"Bien jou�! Le mot �tait "+g.getWord().getString()+" , votre score est de "+g.getPlayer().getPoints()+ " points", "Bon mot", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public String transform(String s, int n, int m) {
		return "Nom : "+ s + " Points " + n + " Essais restants : " +m; 
	}

	
	
	class FramePListener  implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			char rep = ((Button1) e.getSource()).getValue();//On r�cup�re  le caract�re du bouton cliqu�
			System.out.println("Touche enfonc�e " + rep);
			
			System.out.println("Essaie numero "+g.getTries());
			updateFrame();
			
			if(g.getTries()==0) {//plus dessais on arrete tout
				
				theEnd();
			}
			
			else {
				if(g.inTheWord(rep)) {                  //si le caractere est dans le mot mystere
				
				g.play(rep);                           //on met � jour
				g.test();
				updateFrame();
				if(g.win()) {                          //si le mot est trouv�
					
					g.winPoint();                      //On donne les points
					frameWin();
					g.getPlayer().display();
					g.reset();                        //On remet le jeux en marche (nouveau mot, points enregistr�)
					
					updateFrame();
				}else {                            //sinon le caractere nest pas dans le mot mystere
					                   //on met a jour 
					updateFrame();
					System.out.println("BON MOT MAIS PAS JEUX GAGNE");
					g.test();
					System.out.println("Le caractere est dans le mot");
				}
				
				}else {
					g.setTries(g.getTries()-1);
					g.play(rep);//Si le char nest pas dans le mot
					if(g.getTries()-1==0)
						theEnd();
					f.update();
					
				}
			
			
			}
	
	
		
					
		}

	}

}

