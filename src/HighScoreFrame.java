import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Panel;
import java.io.*;

import javax.swing.BorderFactory;
import java.util.*;
import javax.swing.BoxLayout;


public class HighScoreFrame extends JFrame{
	
	public HighScoreFrame(ArrayList<Player> al) {
		this.setTitle("Pendu");
		this.setSize(900, 600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		
		Player[] tabPlayer = new Player[al.size()];
		Collections.sort(al, new SortByPoints());
		
		tabPlayer = al.toArray(tabPlayer);
	
		
		
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		Font font = new Font("Arial",Font.BOLD,30);
		Font font1 = new Font("Arial",Font.BOLD,15);
		
		JLabel top = new JLabel("HIGH SCORE");
		top.setFont(font);
		
		top.setHorizontalAlignment(JLabel.CENTER);
		JPanel topP = new JPanel();
		topP.setBackground(Color.WHITE);
		topP.add(top);
		
		JPanel global = new JPanel();
		
		global.setLayout(new BoxLayout(global, BoxLayout.PAGE_AXIS));
		global.add(topP);
		
		
		JLabel[] tlab = new JLabel[al.size()];
		JPanel[] tpan = new JPanel[al.size()];
		for(int i =0; i<10 && i< al.size(); i++) {
			tlab[i] = new JLabel();
			tpan[i] = new JPanel();
			tlab[i].setPreferredSize(new Dimension(800, 60));
			tlab[i].setHorizontalAlignment(JLabel.CENTER);
			tlab[i].setFont(font1);
			tlab[i].setText(i+1 +". Nom: " + new String(tabPlayer[i].getName()) +  "   Score: " + tabPlayer[i].getFinalPoints()+" points");
			tpan[i].setBackground(Color.WHITE);
			tpan[i].add(tlab[i]);
			global.add(tpan[i]);
			
			
		}
		
		
		

		
		
		
		
		this.add(global);
		this.setVisible(true);
		



	}
	
	

}


