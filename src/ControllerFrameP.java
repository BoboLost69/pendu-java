import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.JOptionPane;
import java.util.ArrayList;


public class ControllerFrameP {
	
	public FrameP f;
	public Game g;
	public String name;
	public ArrayList<Player> al;
	
	public ControllerFrameP(FrameP f, Game g, String s, ArrayList<Player> al) {
		this.f = f;
		this.g = g;
		this.g.getPlayer().setName(s);
		this.f.setInfo(transform(new String(g.getPlayer().getName()), g.getPlayer().getFinalPoints(), g.getTries()));
		this.al =al;

	
	this.f.addFramePListener(new FramePListener());
	}
	
	public void updateFrame() {
		f.setPicture(g.getTries());
		f.setInfo(transform(new String(g.getPlayer().getName()), g.getPlayer().getFinalPoints(), g.getTries()-1));
		f.setMW(g.wordGame().toString());
		//f.revalidate();
		//System.out.println("UPDATE");
	}
	
	public void theEnd() {
		JOptionPane jop1 = new JOptionPane();//on informe du mot et des points gagn�s
		jop1.showMessageDialog(null,"Le mot recherch� �tait "+g.getWord().getString()+" , votre score est de "+g.getPlayer().getFinalPoints()+ " points", "Game Over", JOptionPane.INFORMATION_MESSAGE);
		save(g.getPlayer());
		f.dispose();//remettre les points avant de fermer
	}
	
	public void frameWin() {
		JOptionPane jop1 = new JOptionPane();//on informe du mot et des points gagn�s
		jop1.showMessageDialog(null,"Bien jou�! Le mot �tait "+g.getWord().getString()+" , votre score est de "+g.getPlayer().getPoints()+ " points", "Bon mot", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public String transform(String s, int n, int m) {
		String tmp = "Nom : "+ s + "   Score : " + n + " points   Essais restants : " +m;
		System.out.println(tmp);
		return tmp;
	}

	
	
	class FramePListener  implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			
			
			char rep = ((Button1) e.getSource()).getValue();//On r�cup�re  le caract�re du bouton cliqu�
			System.out.println("Touche enfonc�e " + rep);
			
			System.out.println("Essaie numero "+g.getTries());
			updateFrame();
			
			if(g.getTries()==0) {//plus dessais on arrete tout
				
				updateFrame();
				theEnd();
			}
			
			else {
				if(g.inTheWord(rep)) {                  //si le caractere est dans le mot mystere
				
				g.play(rep);                           //on met � jour
				g.test();
				updateFrame();
				if(g.win()) {                          //si le mot est trouv�
					
					g.winPoint();                      //On donne les points
					frameWin();
					g.getPlayer().display();
					g.reset();                        //On remet le jeux en marche (nouveau mot, points enregistr�)
					f.reset();
					updateFrame();
				}else {                            //sinon le caractere nest pas dans le mot mystere
					                   //on met a jour 
					updateFrame();
					g.test();
				}
				
				}else {
					g.setTries(g.getTries()-1);
					g.play(rep);//Si le char nest pas dans le mot
					f.setPicture(g.getTries());
					if(g.getTries()-1==0) {
						updateFrame();
						theEnd();
					}
					
						
				}
			
			
			}
	
	
		
					
		}

	}
	
	public void save(Player p) {
		
		this.al.add(p);
		System.out.println("SAUVEGARDE!!!!!");
		
		try{
	         FileOutputStream fos= new FileOutputStream("score.dat");
	         ObjectOutputStream oos= new ObjectOutputStream(fos);
	         oos.writeObject(al);
	         oos.close();
	         fos.close();
	       }catch(IOException ioe){
	            ioe.printStackTrace();
	        }
	  }
	}



