import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.Box;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.ArrayList;


//Fenetre Principal 
public class FirstFrame extends JFrame implements ActionListener{
	
	public Button2 go;
	public Button2 about;
	public Button2 highScore;
	public ArrayList<Player> al;

	
  public FirstFrame(){
	  this.setTitle("Pendu");
	  this.setSize(900, 600);
	  this.setLocationRelativeTo(null);
	  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  this.go = new Button2("Jouer");
	  this.about = new Button2("A propos");
	  this.highScore = new Button2("HighScores");
	  this.go.addActionListener(this);
	  this.about.addActionListener(this);
	  this.highScore.addActionListener(this);
	  
	  this.al = new ArrayList<Player>();
	  
	  /**Player a1 = new Player();
	  Player b12 = new Player();
	  Player c1 = new Player();
	  Player d = new Player();
	  a1.setName("Captain");
	  b12.setName("Gelel");
	  c1.setName("Iron Man");
	  d.setName("Nabil");
	  a1.setFinalPoints(15);
	  b12.setFinalPoints(0);
	  c1.setFinalPoints(10);
	  d.setFinalPoints(2983);

	  al.add(a1);
	  al.add(b12);
	  al.add(c1);
	  al.add(d);
		
	  
	  
	  try{
	         FileOutputStream fos= new FileOutputStream("score.dat");
	         ObjectOutputStream oos= new ObjectOutputStream(fos);
	         oos.writeObject(al);
	         oos.close();
	         fos.close();
	       }catch(IOException ioe){
	            ioe.printStackTrace();
	        }
	   
	  
	  **/
	  File f = new File("score.dat");
	  if(f.exists()==false) { 
		  System.out.println("Le fichier nexiste pas on le cr�e");
	     
	  
	  Player a1 = new Player();
	  a1.setName("Nabil");
	  a1.setFinalPoints(576566);
	  this.al.add(a1);
	  Player a2 = new Player();
	  a2.setName("Gelel");
	  a2.setFinalPoints(-20);
	  this.al.add(a2);
		
	  
	  
	  try{
	         FileOutputStream fos= new FileOutputStream("score.dat");
	         ObjectOutputStream oos= new ObjectOutputStream(fos);
	         oos.writeObject(al);
	         oos.close();
	         fos.close();
	       }catch(IOException ioe){
	            ioe.printStackTrace();
	        }
	   
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  }else {
	  
	  
	  	try
      {
	  		System.out.println("Le fichier existe");
          FileInputStream fis = new FileInputStream("score.dat");
          ObjectInputStream ois = new ObjectInputStream(fis);
          this.al = (ArrayList) ois.readObject();
          ois.close();
          fis.close();
       }catch(IOException ioe){
           ioe.printStackTrace();
           
        }catch(ClassNotFoundException c){
           System.out.println("Class not found");
           c.printStackTrace();
           
        }
      for(Player tmp: al){
          tmp.display();
      }
	  }
	  
	  ImagePanel img = new ImagePanel(0);
	  img.setBackground(Color.WHITE);
	  
	  JPanel a = new JPanel();
	  JPanel b = new JPanel();
	  JPanel c = new JPanel();
	  
	  a.setBackground(Color.WHITE);
	  b.setBackground(Color.WHITE);
	  c.setBackground(Color.WHITE);
	  
	  a.add(this.go);
	  b.add(this.about);
	  c.add(this.highScore);
	  
	  Box b1 = Box.createVerticalBox();
	  b1.add(a);
	  b1.add(b);
	  b1.add(c);
	 
	  Box b2 = Box.createHorizontalBox();
	  b2.add(b1);
	  b2.add(img);
	  b2.validate();
	  
	  this.add(b2);
	  
	  this.setVisible(true);
	  
	
	  
	  
	  
	 //Chargement de lensemble des Joueurs dans un tableau
	  
	  
  }
  
  
  

  public void actionPerformed(ActionEvent evt) {
	
	  String rp = evt.getActionCommand();
	  if(rp == "A propos") {
		  new About();
	  }
	  else if(rp == "Jouer") {
		  JOptionPane jop = new JOptionPane();
		  String nom = jop.showInputDialog(null, "Saisissez votre Nom", "Pendu",JOptionPane.QUESTION_MESSAGE);
		  System.out.println(nom);
		  Player p = new Player();
		  p.setName(nom);
		  Game g = new Game(p);
		  FrameP f = new FrameP(g);
		  new ControllerFrameP(f,g,nom, this.al);
		  f.setVisible(true);
	  }
	  else if(rp == "HighScores"){
		  new HighScoreFrame(this.al);
	  }
	  }
  }

