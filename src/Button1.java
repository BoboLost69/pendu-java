import javax.swing.JButton;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Button1 extends JButton implements MouseListener{

	public char c;
	
	public Button1(char c) {
		this.c = c;
		String s;
		s = "" + c;
		this.setText(s);
		this.addMouseListener(this);
	}
	
	public char getValue() {
		return this.c;
	}

	public void mouseClicked(MouseEvent event) { 
		this.setEnabled(false);
		
	}
	
	public void mouseEntered(MouseEvent event) {
		  
	}
	
	public void mouseExited(MouseEvent event) {
		  
	}
	  
	public void mousePressed(MouseEvent event) { }
	  public void mouseReleased(MouseEvent event) { }       

	}

	

