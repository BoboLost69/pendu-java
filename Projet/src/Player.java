
import java.util.*;
import java.io.*;

public class Player implements Serializable{

	int points;// Nombre de points courant
	int sucess;// Nombre de mots trouv�s
	String name;
	int finalPoints;
	
	
	Player(){
		this.points = 0;
		this.sucess = 0;//Nombre de mot trouv� non utilis�
		this.name = " ";
		this.finalPoints=0;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	

	
	public void setSucess(int n) {
		this.sucess = n;
	}
	
	public void setPoints(int n) {
		this.points = n;
	}
	
	public void setName(String s) {
		this.name = s;
	}
	
	public void setFinalPoints(int n) {
		this.finalPoints = n;
	}
	
	public int getPoints(){
		return this.points;
	}
	
	public int getSucess() {
		return this.sucess;
	}
	
	public int getFinalPoints() {
		return this.finalPoints;
		
	}
	public char[] getName() {
		return this.name.toCharArray();
	}
	
	public void display() {
		System.out.println("Nom " + this.name + " Nombre de points: " + this.getFinalPoints());
		System.out.println("*******************************************************");
	}
}
	
	class SortByPoints implements Comparator<Player>
	{
	    // Used for sorting in ascending order of
	    // roll number
	    public int compare(Player a, Player b)
	    {
	        return b.finalPoints - a.finalPoints;
	    }
	}

