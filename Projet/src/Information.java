import javax.swing.JTextArea;
import java.awt.Dimension;

public class Information extends JTextArea {
	
	public String name;
	public int tries;
	public int score;

	public Information(String name, int tries, int score) {
		super();
		this.setText("\n\n  Nom:  " +name+ "\n\n  Nombre d'essais: " + tries + "\n\n  Score: " +score +" points");
		this.setFont(this.getFont().deriveFont(20f));
		this.setEditable(false);
		
	}
	

	
	public void setName(String s) {
		this.name =s;
	}
	
	public void setTries(int n) {
		this.tries =n;
	}
	
	public void setScore(int n) {
		this.score = n;
	}
}
